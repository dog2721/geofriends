import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyA4nE4AMGb8OhiSi4R_cuc2zEApO8J4tMM",
    authDomain: "vue-geofriends.firebaseapp.com",
    databaseURL: "https://vue-geofriends.firebaseio.com",
    projectId: "vue-geofriends",
    storageBucket: "",
    messagingSenderId: "663294789525"
  };
  const firebaseApp = firebase.initializeApp(config);
  firebaseApp.firestore().settings({ timestampsInSnapshots: true })
  export default firebaseApp.firestore()